<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Stringable;

/**
 * LdapRelationInterface interface file.
 * 
 * This class represents a relation between two models, as they are known by
 * that model. Relations do not need not be bi-directional (i.e. it is ok if
 * only one model knows a relation but its couterpart do not know anything).
 * 
 * @author Anastaszor
 * @template T of LdapRecordInterface
 */
interface LdapRelationInterface extends Stringable
{
	
	/**
	 * The relational contant 1-to-1 relation. This means that the relation
	 * is in a state where there can be only one record that is related to this
	 * record according to the key.
	 * This means the related field of this record will fetch only one record
	 * and the field will always return a single record.
	 * The inverse relation of this one is self::ONE_TO_ONE.
	 *
	 * @var string
	 */
	public const ONE_TO_ONE = '1-1';
	
	/**
	 * The relational constant 1-to-n relation. This means that the relation
	 * is in a state where ther can be multiples records that are related to
	 * this record accordingly to the key.
	 * This means the related field of this record will fetch multiple records
	 * and the field will always return an array of records (even if it's empty).
	 * This is the relation to use when the key is a single target key.
	 * The inverse relation of this one is self::MANY_TO_ONE.
	 *
	 * @var string
	 */
	public const ONE_TO_MANY = '1-n';
	
	/**
	 * The relational constant n-to-1 relation. This means that the relation
	 * is in a state where there can be only one record that is related to
	 * this record accordingly to the key.
	 * This means the related field of this record will fetch only one record
	 * and the field will always return a single record.
	 * The inverse relation of this one is self::ONE_TO_MANY or MANY_TO_MANY.
	 *
	 * @var string
	 */
	public const MANY_TO_ONE = 'n-1';
	
	/**
	 * The relational constant n-to-n relation. This means that the relation
	 * is in a state where there can be multiples records that are related to
	 * this record accordingly to the key.
	 * This means the related field of this record will fetch multiple records
	 * and the field will always return an array of records (even if it's empty).
	 * This is the relation to use when the key field is an array of target keys.
	 * As we are not in a db, but in a ldap, the inverse relation of the is
	 * self::MANY_TO_ONE.
	 *
	 * @var string
	 */
	public const MANY_TO_MANY = 'n-n';
	
	/**
	 * Gets the type of relation, one of the constants of this class.
	 * 
	 * @return string
	 */
	public function getTypeOfRelation() : string;
	
	/**
	 * Gets the class of the source model for this relation.
	 *
	 * @return class-string<LdapRecordInterface>
	 */
	public function getSourceClass() : string;
	
	/**
	 * Gets the name of the field in the source model that acs as key for the
	 * relation.
	 * 
	 * @return string
	 */
	public function getSourceField() : string;
	
	/**
	 * Gets the class of the target model for this relation.
	 *
	 * @return class-string<T>
	 */
	public function getTargetClass() : string;
	
	/**
	 * Gets the name of the field in the target model that acs as key for the
	 * relation.
	 * 
	 * @return string
	 */
	public function getTargetField() : string;
	
	/**
	 * Performs the ldap queries to retrieve the records according to this
	 * relation's models and fields.
	 * 
	 * @param LdapRecordInterface $record the source record
	 * @param ?LdapCriteriaInterface $criteria additional filters
	 * @return LdapObjectIteratorInterface<T>
	 */
	public function getRelatedObjects(LdapRecordInterface $record, ?LdapCriteriaInterface $criteria = null) : LdapObjectIteratorInterface;
	
}
