<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Countable;
use Iterator;
use Stringable;

/**
 * LdapEntryIteratorInterface class file.
 * 
 * The Entry Iterator is designed to hold a set of entries that are given
 * from the result of an ldap query.
 *
 * The limit and offset parameters for the query apply here, by bypassing
 * all records under the offset and off the limit.
 *
 * As this interface implements Iterator and Countable, the best use of this
 * object is done into a foreach loop.
 *
 * @author Anastaszor
 * @extends \Iterator<integer, LdapEntryInterface>
 */
interface LdapEntryIteratorInterface extends Countable, Iterator, Stringable
{
	
	/**
	 * Gets whether this query result is empty.
	 *
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Gets the first element of the query.
	 *
	 * @return ?LdapEntryInterface
	 */
	public function getFirstElement() : ?LdapEntryInterface;
	
	/**
	 * Gets the number of entries that the query has matched for the 
	 * appropriate criteria.
	 * 
	 * @return integer
	 */
	public function getQueryCount() : int;
	
	/**
	 * Builds a new LdapObjectIteratorInterface from this result and the other
	 * given result.
	 *
	 * @param LdapEntryIteratorInterface $result
	 * @return LdapEntryIteratorInterface
	 */
	public function mergeWith(LdapEntryIteratorInterface $result) : LdapEntryIteratorInterface;
	
}
