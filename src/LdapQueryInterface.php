<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Stringable;

/**
 * LdapQueryInterface interface file.
 * 
 * This class represents a single ldap search call to be done. It embeds all
 * the needed information for it to be done and can be repeated any time to
 * give a newer result set. Those objects should be immutable.
 * 
 * @author Anastaszor
 */
interface LdapQueryInterface extends Stringable
{
	
	/**
	 * Gets the base distinguished name on which this query holds.
	 * 
	 * @return LdapDistinguishedNameInterface
	 */
	public function getBaseDn() : LdapDistinguishedNameInterface;
	
	/**
	 * Gets the filter that is applicable to this query.
	 * 
	 * @return LdapFilterNodeMultiInterface
	 */
	public function getFilter() : LdapFilterNodeMultiInterface;
	
	/**
	 * Gets the scope that is applicable to this query.
	 * 
	 * @return integer
	 */
	public function getScope() : int;
	
	/**
	 * Gets the offset that is applicable to this query.
	 * 
	 * @return integer
	 */
	public function getOffset() : int;
	
	/**
	 * Gets the limit that is applicable to this query.
	 * 
	 * @return integer
	 */
	public function getLimit() : int;
	
	/**
	 * Gets the result of the execution of the query from the ldap. Each time
	 * this method is called, a new query should be executed on the ldap, no
	 * caching mecanism should be present inside this query object.
	 * 
	 * @return LdapQueryResultInterface
	 */
	public function getResult() : LdapQueryResultInterface;
	
}
