<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Countable;
use Stringable;

/**
 * LdapQueryResultInterface interface file.
 * 
 * This represents the result of a newly executed query from the ldap. It 
 * provides the result of the ldap query and a method to retrieve the effective
 * list of entries according to the parameters that were passed to the query
 * (in particular applying the limit and offset of the query to the result set
 * that is returned by the ldap library).
 * 
 * @author Anastaszor
 */
interface LdapQueryResultInterface extends Countable, Stringable
{
	
	/**
	 * Gets if this has an error, i.e. if it has been executed and the error
	 * code is not a non-error code.
	 *
	 * @return boolean
	 */
	public function hasError() : bool;
	
	/**
	 * Gets the error code that was given by the ldap on the last call.
	 *
	 * @return integer
	 */
	public function getError() : int;
	
	/**
	 * Gets the number of records that the query has matched for the
	 * appropriate criteria.
	 *
	 * @return integer
	 */
	public function getQueryCount() : int;
	
	/**
	 * Gets the entries that were available from this query result.
	 * 
	 * @return LdapEntryIteratorInterface
	 */
	public function getEntries() : LdapEntryIteratorInterface;
	
}
