<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Stringable;

/**
 * LdapDirectoryInterface interface file.
 *
 * This interface represents a session of a given user that may be stored
 * into any persistent storage support (such as file, db session, or php
 * native session mechanism).
 *
 * @author Anastaszor
 */
interface LdapUserSessionInterface extends Stringable
{
	
	/**
	 * Gets a new session from the given username and password. If the username
	 * is null, then this session is anonymous.
	 *
	 * @param ?string $username
	 * @param ?string $password
	 * @return LdapUserSessionInterface
	 */
	public static function create(?string $username = null, ?string $password = null) : LdapUserSessionInterface;
	
	/**
	 * Loads this session from wherever it is saved. This method returns null
	 * if there is no session availale to load.
	 *
	 * @return ?LdapUserSessionInterface
	 */
	public static function load() : ?LdapUserSessionInterface;
	
	/**
	 * Gets whether current session user is anonymous.
	 *
	 * @return boolean
	 */
	public function isAnonymous() : bool;
	
	/**
	 * Gets the identifier of the user, which the ldap directory should be able
	 * to transform into a suitable distinguished name to be logged with.
	 *
	 * @return ?string
	 */
	public function getIdentifier() : ?string;
	
	/**
	 * Gets the password of the user, clear text. This password should be stored
	 * encrypted.
	 *
	 * @return ?string
	 */
	public function getPassword() : ?string;
	
	/**
	 * Saves this session to wherever it must be saved. This method returns
	 * true if the session is sucessfully saved, and false if it fails.
	 *
	 * @return boolean
	 */
	public function save() : bool;
	
	/**
	 * Renders this session anonymous, without dn nor usable password, and
	 * clears those credentials in the session data, then saves the session.
	 * This method returns true if the save is successful and false if it fails.
	 *
	 * @return boolean
	 */
	public function logout() : bool;
	
}
