<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Stringable;

/**
 * LdapConfigurationInterface interface file.
 * 
 * This interface specifies how the configuration data files should be built
 * for an ldap connection.
 * 
 * @author Anastaszor
 */
interface LdapConfigurationInterface extends Stringable
{
	
	/**
	 * Gets the hostname to which to connect or the ip address of the host.
	 * 
	 * @return string
	 */
	public function getHostname() : string;
	
	/**
	 * Gets the TCP or UDP port number to which to connect.
	 * 
	 * @return integer
	 */
	public function getPort() : int;
	
	/**
	 * Gets whether to use ldaps over classical ldap protocol.
	 * 
	 * @return boolean
	 */
	public function isSecure() : bool;
	
	/**
	 * Gets the connection timout in seconds.
	 * 
	 * @return integer
	 */
	public function getConnectionTimeout() : int;
	
	/**
	 * Gets the suffix of the directory. This is the top part of every 
	 * distinguished name that should be searched.
	 * 
	 * @return LdapDistinguishedNameInterface
	 */
	public function getSuffixDn() : LdapDistinguishedNameInterface;
	
	/**
	 * Gets the distinguished name used for authentication.
	 * 
	 * @return ?string
	 */
	public function getLogin() : ?string;
	
	/**
	 * Gets the cleartext password used for authentication.
	 * 
	 * @return ?string
	 */
	public function getPassword() : ?string;
	
	/**
	 * Gets the version of the ldap that should be used for the connection.
	 * 
	 * @return integer between 1 and 3, inclusive
	 */
	public function getVersion() : int;
	
	/**
	 * Gets whether this client should follow the referrals automatically.
	 * 
	 * @return boolean
	 */
	public function followsReferral() : bool;
	
	/**
	 * Gets how aliases should be derefenced. Possible values mean :
	 * 		0 - NEVER
	 * 		1 - SEARCHING
	 * 		2 - FINDING
	 * 		3 - ALWAYS.
	 * 
	 * @return integer between 0 and 3, inclusive
	 */
	public function getDereferencePolicy() : int;
	
	/**
	 * Gets the maximum number of entries that will be returned. Zero is no
	 * limit.
	 * 
	 * @return integer
	 */
	public function getSizeLimit() : int;
	
	/**
	 * Gets the maximum time, in milliseconds, for any ldap operation. Zero is
	 * no limit.
	 * 
	 * @return integer
	 */
	public function getTimeLimit() : int;
	
	/**
	 * Whether the the connection should ping the ldap before attempting a
	 * connection (this should add performance improvements on failures).
	 * 
	 * @return boolean
	 */
	public function isServicePingEnabled() : bool;
	
}
