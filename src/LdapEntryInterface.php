<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Stringable;

/**
 * LdapEntryInterface interface file.
 * 
 * This interface specifies a wrapper for the resulting data that is to be
 * extracted from the ldap_entry resource structure. If such extraction is
 * required, the distinguished name of the entry, as well as its objectClasses
 * and its other attributes are retrieved and stored into this wrapper.
 * 
 * This generic object exist to be transformed by an LdapObjectFactory to
 * a real relational object by inspecting its 
 * 
 * @author Anastaszor
 */
interface LdapEntryInterface extends Stringable
{
	
	/**
	 * Returns the distinguished name of the fetched entry, or null if there
	 * was an error.
	 * 
	 * @return LdapDistinguishedNameInterface
	 */
	public function getDn() : LdapDistinguishedNameInterface;
	
	/**
	 * Gets whether this entry has the given objectClass.
	 * 
	 * @param string $objectClass
	 * @return boolean
	 */
	public function hasObjectClass(string $objectClass) : bool;
	
	/**
	 * Gets the object classes of the entry as values in the returned array.
	 * 
	 * @return array<int, string>
	 */
	public function getObjectClasses() : array;
	
	/**
	 * Gets whether the attribute with the given attribute name exist in this
	 * entry, regardless of its value.
	 * 
	 * @param string $attributeName
	 * @return boolean
	 */
	public function hasAttribute(string $attributeName) : bool;
	
	/**
	 * Gets the attribute value with the given attribute name.
	 * 
	 * @param string $attributeName
	 * @return null|int|float|string|array<int, int|float|string>
	 */
	public function getAttribute(string $attributeName);
	
	/**
	 * Returns all the attributes of the fetched entry, or empty array if
	 * there was an error. This gives back the raw ldap array of entries,
	 * filtered without the count values and with only the attributed values,
	 * as the string key is the value of the attribute.
	 *
	 * Beware that certain ldaps lowercase some of the attributes that are
	 * returned by this method.
	 *
	 * @return array<string, int|float|string|array<int, int|float|string>>
	 */
	public function getAttributes() : array;
	
}
