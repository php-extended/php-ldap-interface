<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Countable;
use Iterator;
use Stringable;

/**
 * LdapObjectIteratorInterface class file.
 * 
 * This Object Iterator is an iterator that iterates over entries that are
 * or will be transformed into suitable objects via the ldap object factory.
 * 
 * As this interface implements Iterator and Countable, the best use of this 
 * object is done into a foreach loop.
 * 
 * @author Anastaszor
 * @template T of LdapRecordInterface
 * @extends \Iterator<int, T>
 */
interface LdapObjectIteratorInterface extends Countable, Iterator, Stringable
{
	
	/**
	 * Gets whether this query result is empty.
	 * 
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Gets the first element of the query.
	 * 
	 * @return ?T
	 */
	public function getFirstElement() : ?LdapRecordInterface;
	
	/**
	 * Gets the number of records that the query has matched for the 
	 * appropriate criteria.
	 * 
	 * @return integer
	 */
	public function getQueryCount() : int;
	
	/**
	 * Builds a new LdapObjectIteratorInterface from this result and the other 
	 * given result.
	 * 
	 * @param LdapObjectIteratorInterface<T> $result
	 * @return LdapObjectIteratorInterface<T>
	 */
	public function mergeWith(LdapObjectIteratorInterface $result) : LdapObjectIteratorInterface;
	
}
