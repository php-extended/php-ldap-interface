<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Stringable;

/**
 * LdapRecord class file.
 *
 * This class serves as a base for each record class that uses the LdapConnection
 * to the ldap server. This class provides multiple functions that
 * simplifies the management of an abstract object relational mapping
 * structure (like in a database ORM framework).
 * 
 * If some models needs to use a different connection instance than the standard
 * default one, please override the getConnection() method.
 * 
 * @author Anastaszor
 */
interface LdapRecordInterface extends Stringable
{
	
	/**
	 * Gets the ldap connection object.
	 * 
	 * @return LdapConnectionInterface
	 * @throws LdapThrowable if there is no such connection available
	 */
	public static function getConnection() : LdapConnectionInterface;
	
	/**
	 * Gets the name of the ldap object class that most represents the object.
	 * This method must return a single string and is not the same as the
	 * objectClass field, which returns an array of classes in which this
	 * object belongs.
	 * 
	 * @return string
	 */
	public static function getLdapObjectClass() : string;
	
	/**
	 * Gets the base distinguished names in which the search should find objects
	 * for the given class. This is taken as a scope, saying all object of this
	 * class should be included into the subtrees of all the given distinguished
	 * names.
	 * 
	 * @return array<integer, LdapDistinguishedNameInterface>
	 */
	public static function getLdapBaseDistinguishedNames() : array;
	
	/**
	 * Gets an additional criteria filter in case of the filter on the
	 * ldapObjectClass is not filtering enough for the searches. This can
	 * be useful to distinguishing between ldap object classes and your php
	 * object classes is not enough clear (particularly if some classes are
	 * only present in a specific OUs and the filter needs to be specifed to
	 * not hit the administrative limit exceeded error).
	 * 
	 * @return array<integer, LdapFilterNodeInterface>
	 */
	public static function getLdapClassCriteriaFilters() : array;
	
	/**
	 * The identifier final field of the record. We know that the distinguished
	 * name is the core identifier as no records should have the same dn, the
	 * identifier field is only the field representing the last leaf in the
	 * tree given by the dn. Usually, it is cn (common name) or uid (user
	 * identifier), but in case of ou's it can be an ou (organisational unit),
	 * and so on.
	 * 
	 * @return string
	 */
	public static function getIdentifierField() : string;
	
	/**
	 * The unique fields are fields that may be used as key when searching for
	 * models instead of the identifier or the dn. This function returns an
	 * array with all of those fields names.
	 * 
	 * @return array<integer, string>
	 */
	public static function getUniqueFields() : array;
	
	/**
	 * This function gets back all the attributes of all objectClasses that
	 * compose this object. The following classes are available :
	 * - int / integer
	 * - float / double
	 * - bool / boolean
	 * - string
	 * - array([type]) / type[] : Warning, if the array() syntax is used, we
	 * 		must specify the internal type, which may be any one of the previous,
	 * 		except array(). In case [type] is not specified, it will be silently
	 * 		converted to string. The brackets in "[type]" are optional, whereas in
	 * 		type[] they mark the fact it is an array (mandatory).
	 * - [objectClass] : If object is used, the type should be a system loadable
	 * 		class name. If this is used, the constructor's signature for this
	 * 		object must be : public function __construct(null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $type): void ; because
	 * 		the first parameters will be automatically given as $type to the
	 * 		constructor. The brackets in "[objectClass]" are not needed.
	 * If the attribute classes are not parseable, exceptions will be thrown
	 * by the LdapObjectFactory.
	 * 
	 * Attributes which are not in this list will be parsed as plain string.
	 * If an element is multi valued and is not specified as an array, depending
	 * of the factory, an exception may be thrown.
	 * 
	 * If this method is inherited (and it should be), they SHOULD be
	 * implemented using :
	 * array_merge(parent::getAttributeClasses(), [ ... ])
	 * to get all the rules.
	 * 
	 * @return array<string, string>
	 */
	public static function getAttributesClasses() : array;
	
	/**
	 * The attribute names of fields of this class.
	 *
	 * The keys of the array must be the lowercase version of the attributes
	 * names, while the values of the array must be the properly cased
	 * attribute names for this object.
	 *
	 * @return array<string, string>
	 */
	public static function attributes() : array;
	
	/**
	 * Returns all the attribute labels by attribute name for this class.
	 *
	 * The keys of the array must be the real case attribute name and the values
	 * of the array must be the label value of the model field.
	 *
	 * @return array<string, string>
	 */
	public static function labels() : array;
	
	/**
	 * Gets the label for given attribute. If no label is available, this
	 * returns the name of the attribute.
	 *
	 * @param string $attribute
	 * @return string
	 */
	public static function getLabel(string $attribute) : string;
	
	/**
	 * Returns all the help texts by attribute name for this class.
	 *
	 * @return array<string, string>
	 */
	public static function helpTexts() : array;
	
	/**
	 * Gets the help text for given attribute. If no help text is available,
	 * this returns an empty string.
	 *
	 * @param string $attribute
	 * @return string
	 */
	public static function getHelpText(string $attribute) : string;
	
	/**
	 * Gets the relations that this record is aware of.
	 * 
	 * @return array<string, LdapRelationInterface<LdapRecordInterface>>
	 */
	public static function relations() : array;
	
	/**
	 * Finds an element by its dn. Like its dn is a primary key, there should
	 * not be multiple records within a dn. Searching by dn requires to have
	 * exacts dn to search by, jokers are not allowed.
	 * 
	 * @param LdapDistinguishedNameInterface $dname
	 * @param ?LdapCriteriaInterface $criteria
	 * @return ?static
	 */
	public static function findByDn(LdapDistinguishedNameInterface $dname, ?LdapCriteriaInterface $criteria) : ?LdapRecordInterface;
	
	/**
	 * Finds multiple elements based on attributes filters. Those attributes
	 * will be threated as AND conditions. For more sophisticated queries,
	 * use the LdapCriteria object directly.
	 * 
	 * @param array<string, string> $attributes
	 * @return LdapObjectIteratorInterface<static>
	 */
	public static function findByAttributes(array $attributes = []) : LdapObjectIteratorInterface;
	
	/**
	 * Find a set of objects that match this class and given criteria.
	 * 
	 * @param ?LdapCriteriaInterface $criteria
	 * @return LdapObjectIteratorInterface<static>
	 */
	public static function findAll(?LdapCriteriaInterface $criteria) : LdapObjectIteratorInterface;
	
	/**
	 * Find a set of objects that match this class and given attributes. All
	 * of those attributes will be treated as an AND filter. For more
	 * sophisticated queries, use the LdapCriteria object directly.
	 * 
	 * @param array<string, string> $attributes
	 * @return LdapObjectIteratorInterface<static>
	 */
	public static function findAllByAttributes(array $attributes = []) : LdapObjectIteratorInterface;
	
	/**
	 * Count all objects that matches this class and given criteria.
	 * 
	 * @param ?LdapCriteriaInterface $criteria
	 * @return integer
	 */
	public static function count(?LdapCriteriaInterface $criteria) : int;
	
	/**
	 * Count all objects that matches this class and given attributes. All
	 * attributes filters will be treated as an AND filter. For more
	 * sophisticated queries, use the LdapCriteria object directly.
	 * 
	 * @param array<string, string> $attributes
	 * @return integer
	 */
	public static function countByAttributes(array $attributes = []) : int;
	
	/**
	 * Gets the value of the given attribute.
	 *
	 * @param string $name
	 * @return null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>>
	 * @throws LdapThrowable
	 */
	public function __get(string $name);
	
	/**
	 * Sets the value of the given attribute.
	 *
	 * @param string $name
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $value
	 * @throws LdapThrowable
	 */
	public function __set(string $name, $value) : void;
	
	/**
	 * Returns whether the given attribute has a value.
	 *
	 * @param string $name
	 * @return boolean
	 */
	public function __isset(string $name) : bool;
	
	/**
	 * Unsets the value of the given attribute.
	 *
	 * @param string $name
	 * @throws LdapThrowable
	 */
	public function __unset(string $name) : void;
	
	/**
	 * Returns true if this object and the given object model is equal to this
	 * one.
	 *
	 * @param null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object|array<integer|string, null|boolean|integer|float|string|object>> $object
	 * @return boolean
	 */
	public function equals($object) : bool;
	
	/**
	 * Gets the unique distinguished name of this object.
	 * 
	 * @return LdapDistinguishedNameInterface
	 */
	public function getDn() : LdapDistinguishedNameInterface;
	
	/**
	 * Gets the object classes of this object.
	 *
	 * @return array<integer, string>
	 */
	public function getObjectClasses() : array;
	
	/**
	 * Verifies if the argument record is in the subtree of this record.
	 * 
	 * @param LdapRecordInterface $record
	 * @return boolean
	 */
	public function contains(LdapRecordInterface $record) : bool;
	
	/**
	 * Verifies if this record is in the subtree of the argument record.
	 * 
	 * @param LdapRecordInterface $record
	 * @return boolean
	 */
	public function isContainedBy(LdapRecordInterface $record) : bool;
	
	/**
	 * Returns wether or not the record is a leaf, meaning the record can't
	 * have children records.
	 * 
	 * @return boolean
	 */
	public function isLeaf() : bool;
	
	/**
	 * Gets the parent record of this one. We cannot determine statically which
	 * class the parent is from, but the factory will. This method returns null
	 * if there is no such parent.
	 * 
	 * @return ?LdapRecordInterface
	 */
	public function getParent() : ?LdapRecordInterface;
	
	/**
	 * Gets all the direct children for given model, in given class. This 
	 * gathers only children from one level deep.
	 * 
	 * @template T of LdapRecordInterface
	 * @param ?class-string<T> $childClass the model to filter by
	 * @return LdapObjectIteratorInterface<T>
	 */
	public function getChildren(?string $childClass = null) : LdapObjectIteratorInterface;
	
	/**
	 * Counts the number of children in given model, in given class.
	 * 
	 * @template T of LdapRecordInterface
	 * @param ?class-string<T> $childClass the model to filter by
	 * @return integer
	 */
	public function getChildrenCount(?string $childClass = null) : int;
	
	/**
	 * Gets all children from the whole subtree for given model, in given
	 * class.
	 * 
	 * @template T of LdapRecordInterface
	 * @param ?class-string<T> $childClass the model to filter by
	 * @return LdapObjectIteratorInterface<T>
	 */
	public function getSubtreeChildren(?string $childClass = null) : LdapObjectIteratorInterface;
	
	/**
	 * Counts the number of children in the subtree given model, in given class.
	 * 
	 * @template T of LdapRecordInterface
	 * @param ?class-string<T> $childClass the model to filter by
	 * @return integer
	 */
	public function getSubtreeChildrenCount(?string $childClass = null) : int;
	
	/**
	 * Find only direct children of this element, that match given criteria.
	 * 
	 * @param ?LdapCriteriaInterface $criteria
	 * @return LdapObjectIteratorInterface<LdapRecordInterface>
	 */
	public function findChildren(?LdapCriteriaInterface $criteria) : LdapObjectIteratorInterface;
	
	/**
	 * Find only direct children of this element, that match given attributes.
	 * All of those attributes filters will be threated as AND filters. For
	 * more sophisticated queries, use the LdapCriteria object directly.
	 * 
	 * @param array<string, string> $attributes
	 * @return LdapObjectIteratorInterface<LdapRecordInterface>
	 */
	public function findChildrenByAttributes(array $attributes = []) : LdapObjectIteratorInterface;
	
	/**
	 * Find all children of the subtree of this element, that matches given
	 * criteria.
	 * 
	 * @param ?LdapCriteriaInterface $criteria
	 * @return LdapObjectIteratorInterface<LdapRecordInterface>
	 */
	public function findSubtreeChildren(?LdapCriteriaInterface $criteria) : LdapObjectIteratorInterface;
	
	/**
	 * Find all children of the subtree of this element, that match given
	 * attributes. All of those attributes filters will be threated as AND
	 * filters. For more sophisticated queries, use the LdapCriteriaInterface
	 * object directly.
	 * 
	 * @param array<string, string> $attributes
	 * @return LdapObjectIteratorInterface<LdapRecordInterface>
	 */
	public function findSubtreeChildrenByAttributes(array $attributes = []) : LdapObjectIteratorInterface;
	
	/**
	 * Counts the number direct children of this class that matches argument
	 * criteria.
	 * 
	 * @param ?LdapCriteriaInterface $criteria
	 * @return integer
	 */
	public function countChildren(?LdapCriteriaInterface $criteria) : int;
	
	/**
	 * Counts the number of children in the subtree of this class that matches
	 * argument criteria.
	 * 
	 * @param ?LdapCriteriaInterface $criteria
	 * @return integer
	 */
	public function countSubtreeChildren(?LdapCriteriaInterface $criteria) : int;
	
}
