<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Stringable;

/**
 * LdapCriteriaInterface interface file.
 * 
 * This interface specifies how objects that specify constraints on how the
 * searches on the ldap should be constructed to be able to search for records
 * in the ldap. This manages the base distinguished name of the ldap, the
 * scope of the search, and all the filters that will apply to the search.
 * 
 * The ldap criteria behaves like an AND filter node (for the filter part).
 * 
 * @author Anastaszor
 */
interface LdapCriteriaInterface extends Stringable
{
	
	// scope to search at specified dn (ldap_read)
	public const LDAP_SCOPE_BASE = 0;
	// scope to search all children of specified dn (ldap_list)
	public const LDAP_SCOPE_ONE = 1;
	// scope to search all subtree of specified dn (ldap_search)
	public const LDAP_SCOPE_SUBTREE = 2;
	// how many records should be returned for the tree and list queries
	public const DEFAULT_LIMIT = 1000;
	
	/**
	 * Gets whether this criteria is empty, meaning it contains no criteria
	 * filters.
	 *
	 * @return boolean
	 */
	public function isEmpty() : bool;
	
	/**
	 * Sets the number of records that should be skipped until we reach the
	 * ones we want to fetch. Zero or negative is no offset.
	 * 
	 * @param integer $offset
	 * @return LdapCriteriaInterface self
	 */
	public function setOffset(int $offset) : LdapCriteriaInterface;
	
	/**
	 * Gets the offset value of this criteria.
	 *
	 * @return integer
	 */
	public function getOffset() : int;
	
	/**
	 * Sets the limit to the number of records to fetch. Negative is no limit,
	 * and zero is effectively no records. Note that this value will be
	 * overridden by the size limit setting in the configuration of the
	 * connection if it is too high (this is to be able to query without
	 * reaching the administrative limit if possible).
	 * 
	 * @param integer $limit
	 * @return LdapCriteriaInterface self
	 */
	public function setLimit(int $limit) : LdapCriteriaInterface;
	
	/**
	 * Gets the limit value of this criteria.
	 *
	 * @return integer
	 */
	public function getLimit() : int;
	
	/**
	 * Sets the scope of the search, using the scope constants of this class.
	 *
	 * @param integer $scope from 0 to 2, inclusive
	 * @return LdapCriteriaInterface self
	 */
	public function setScope(int $scope) : LdapCriteriaInterface;
	
	/**
	 * Gets the scope of the search.
	 *
	 * @return integer
	 */
	public function getScope() : int;
	
	/**
	 * Sets the model class that should be used to filter query records.
	 * 
	 * @param class-string<LdapRecordInterface> $recordClass
	 * @return LdapCriteriaInterface
	 */
	public function setRecordClass(string $recordClass) : LdapCriteriaInterface;
	
	/**
	 * Gets the model class that should be used to filter records. If no model
	 * is specified, then this will search for objects of all classes.
	 *
	 * @return ?class-string<LdapRecordInterface>
	 */
	public function getRecordClass() : ?string;
	
	/**
	 * Adds the base distinguished name to the existing ones in which tree
	 * to search objects.
	 * 
	 * @param LdapDistinguishedNameInterface $baseDn
	 * @return LdapCriteriaInterface self
	 */
	public function addBaseDn(LdapDistinguishedNameInterface $baseDn) : LdapCriteriaInterface;
	
	/**
	 * Removes existing base distinguished names, then sets the base
	 * distinguished name in which tree to search objects.
	 *
	 * @param LdapDistinguishedNameInterface $baseDn
	 * @return LdapCriteriaInterface self
	 */
	public function setBaseDn(LdapDistinguishedNameInterface $baseDn) : LdapCriteriaInterface;
	
	/**
	 * Adds all the base distinguished names to the existing ones in which
	 * trees to search objects.
	 * 
	 * @param array<integer, LdapDistinguishedNameInterface> $baseDns
	 * @return LdapCriteriaInterface self
	 */
	public function addAllBaseDns(array $baseDns) : LdapCriteriaInterface;
	
	/**
	 * Removes existing base distinguished names, then sets all the base
	 * distinguished names in which trees to search objects.
	 *
	 * @param array<integer, LdapDistinguishedNameInterface> $baseDns
	 * @return LdapCriteriaInterface self
	 */
	public function setAllBaseDns(array $baseDns) : LdapCriteriaInterface;
	
	/**
	 * Gets all the base distinguished names that indicates the subtrees in 
	 * which to search objects.
	 * 
	 * @return array<integer, LdapDistinguishedNameInterface>
	 */
	public function getBaseDns() : array;
	
	/**
	 * Sets the filter root nodes by replacing completely the existing filter
	 * node hierarchy.
	 * 
	 * @param LdapFilterNodeMultiInterface $rootNode
	 * @return LdapCriteriaInterface
	 */
	public function setFilters(LdapFilterNodeMultiInterface $rootNode) : LdapCriteriaInterface;
	
	/**
	 * Adds a node to this filter.
	 * 
	 * @param LdapFilterNodeInterface $node
	 * @return LdapCriteriaInterface
	 */
	public function addFilterNode(LdapFilterNodeInterface $node) : LdapCriteriaInterface;
	
	/**
	 * Adds the nodes to this filter.
	 * 
	 * @param array<integer, LdapFilterNodeInterface> $nodes
	 * @return LdapCriteriaInterface
	 */
	public function addFilterNodes(array $nodes) : LdapCriteriaInterface;
	
	/**
	 * Adds a single value to this criteria.
	 * 
	 * @param string $column
	 * @param null|bool|integer|float|string $value
	 * @param string $comparator
	 * @return LdapCriteriaInterface
	 */
	public function addValue(string $column, $value, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapCriteriaInterface;
	
	/**
	 * Adds a single value that is negated to this criteria.
	 * 
	 * @param string $column
	 * @param null|bool|integer|float|string $value
	 * @param string $comparator
	 * @return LdapCriteriaInterface
	 */
	public function addNotValue(string $column, $value, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapCriteriaInterface;
	
	/**
	 * Adds multiple values with an OR junction to this criteria.
	 * 
	 * @param string $column
	 * @param array<integer, null|bool|integer|float|string> $values
	 * @param string $comparator
	 * @return LdapCriteriaInterface
	 */
	public function addValues(string $column, array $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapCriteriaInterface;
	
	/**
	 * Adds multiple values with an OR junction that are negated to this criteria.
	 * 
	 * @param string $column
	 * @param array<integer, null|bool|integer|float|string> $values
	 * @param string $comparator
	 * @return LdapCriteriaInterface
	 */
	public function addNotValues(string $column, array $values, string $comparator = LdapFilterNodeValueInterface::CMP_EQUALS) : LdapCriteriaInterface;
	
	/**
	 * Removes all the nodes that have the given column as comparison. This
	 * method removes also all the nested filter nodes with the given column.
	 * 
	 * @param string $column
	 * @return LdapCriteriaInterface
	 */
	public function removeFiltersFor(string $column) : LdapCriteriaInterface;
	
	/**
	 * Gets the node tree that represents the current filters to be used in the
	 * ldap functions without the filter that represents the object class.
	 * 
	 * @return LdapFilterNodeMultiInterface
	 */
	public function getFilters() : LdapFilterNodeMultiInterface;
	
	/**
	 * Gets the node tree that represents all the filters to be used in the ldap
	 * functions. This includes the filter that is given by the object model,
	 * if specified.
	 *
	 * @return LdapFilterNodeMultiInterface
	 */
	public function getAllFilters() : LdapFilterNodeMultiInterface;
	
	/**
	 * Creates a new criteria which is the merge between this criteria and the
	 * other given criteria.
	 * 
	 * @param LdapCriteriaInterface $criteria
	 * @return LdapCriteriaInterface
	 */
	public function mergeWith(LdapCriteriaInterface $criteria) : LdapCriteriaInterface;
	
}
