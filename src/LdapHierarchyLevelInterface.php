<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Stringable;

/**
 * LdapHierarchyLevelHierarchy interface file.
 * 
 * This interface specifies a specific level of hierarchy for a given record.
 * 
 * @author Anastaszor
 * @template T of LdapRecordInterface
 */
interface LdapHierarchyLevelInterface extends Stringable
{
	
	/**
	 * Gets the target at this level of the hierarchy.
	 * 
	 * @return T
	 */
	public function getTarget() : LdapRecordInterface;
	
	/**
	 * Gets whether this level is the first level of the hierarchy (i.e. the
	 * least profound).
	 * 
	 * @return boolean
	 */
	public function isFirst() : bool;
	
	/**
	 * Gets whether this level is the last level of the hierarchy (i.e. the
	 * most profound).
	 * 
	 * @return boolean
	 */
	public function isLast() : bool;
	
	/**
	 * Gets the current level of the record, relative to the level of the
	 * first level in this hierarchy.
	 * 
	 * @return integer
	 */
	public function getLevel() : int;
	
	/**
	 * Gets the neighbors of the target (children of the same parent that
	 * are the same class) represented by the given criteria. If there is no
	 * criteria given, all the records are retrieved.
	 * 
	 * @param ?LdapCriteriaInterface $criteria
	 * @return LdapObjectIteratorInterface<T>
	 */
	public function getNeighbors(?LdapCriteriaInterface $criteria) : LdapObjectIteratorInterface;
	
}
