<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Stringable;

/**
 * LdapDirectoryInterface interface file.
 *
 * This interface represents a physical endpoint for an ldap directory, or
 * multiple directories that may be used differently based on the used
 * configuration file.
 *
 * @author Anastaszor
 */
interface LdapDirectoryInterface extends Stringable
{
	
	/**
	 * Gets the base distinguished name for all the records based on the
	 * current configuration file.
	 *
	 * @return LdapDistinguishedNameInterface
	 */
	public function getBaseDn() : LdapDistinguishedNameInterface;
	
	/**
	 * Checks whether the given username and password can log in to the ldap,
	 * then closes the connection.
	 *
	 * @param LdapUserSessionInterface $user
	 * @return boolean
	 * @throws LdapThrowable
	 */
	public function checkLogin(LdapUserSessionInterface $user) : bool;
	
	/**
	 * Connects (binds) the given username and password to the ldap into a
	 * persistent connection, then returns the connection opened with the
	 * credentials of the given user session.
	 *
	 * @param LdapUserSessionInterface $user
	 * @return LdapConnectionInterface
	 * @throws LdapThrowable
	 */
	public function connect(LdapUserSessionInterface $user) : LdapConnectionInterface;
	
	/**
	 * Checks whether the given username has the rights for the given
	 * application and role. This method requires a successful connect before
	 * being executed, and the logged-in user should have visibility over the
	 * applications and roles accesses, otherwise false will be returned.
	 *
	 * @param LdapUserSessionInterface $user
	 * @param ?string $application
	 * @param ?string $role
	 * @return boolean
	 * @throws LdapThrowable
	 */
	public function checkAccess(LdapUserSessionInterface $user, ?string $application = null, ?string $role = null) : bool;
	
	/**
	 * Guess the most probable dn from the given username.
	 *
	 * @param string $username
	 * @return LdapDistinguishedNameInterface
	 * @throws LdapThrowable
	 */
	public function guessDnFromUsername(?string $username) : LdapDistinguishedNameInterface;
	
	/**
	 * Builds a given hierarchy from a specific record.
	 *
	 * @param LdapRecordInterface $record
	 * @return LdapHierarchyInterface
	 * @throws LdapThrowable
	 */
	public function createHierarchy(LdapRecordInterface $record) : LdapHierarchyInterface;
	
}
