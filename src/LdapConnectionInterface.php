<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Stringable;

/**
 * LdapConnectionInterface interface file.
 * 
 * This interface represents the link to the ldap resource. This resource can be
 * in three distinct states : "uninitialized", "connected" or "connection failed".
 * 
 * The "uninitialized" state is the default state. The connection will try to
 * establish itself as soon as we will try to fetch data from the ldap. In case
 * the connection fail, the connection will pass into the "connection failed"
 * state and throw an exception. When in "connection failed" state, all methods
 * that retrieve data will silently return 0 as count of elements, and an empty
 * set any time needed. On the other hand, if the connection is succesful, the
 * connection will pass into the "connected" state, which is the nominal state
 * of this class, when the connection can be used to retrieved data through the
 * link.
 * 
 * The inherited classes should supports pagination, even if it is not natively
 * supported because of your version of php or php-ldap library, or it is not
 * supported by the ldap server. It is clear that using pagination on a system
 * that does not natively supports it should be used carefully to avoid large
 * performance penalties.
 * 
 * @author Anastaszor
 */
interface LdapConnectionInterface extends Stringable
{
	
	/**
	 * @see http://wiki.servicenow.com/index.php?title=LDAP_Error_Codes#gsc.tab=0
	 */
	public const LDAP_SUCCESS = 0;
	public const LDAP_OPERATIONS_ERROR = 1;
	public const LDAP_PROTOCOL_ERROR = 2;
	public const LDAP_TIMELIMIT_EXCEEDED = 3;
	public const LDAP_SIZELIMIT_EXCEEDED = 4;
	public const LDAP_COMPARE_FALSE = 5;
	public const LDAP_COMPARE_TRUE = 6;
	public const LDAP_AUTH_METHOD_NOT_SUPPORTED = 7;
	public const LDAP_STRONG_AUTH_REQUIRED = 8;
	public const LDAP_REFERRAL = 10;
	public const LDAP_ADMINLIMIT_EXCEEDED = 11;
	public const LDAP_UNAVAILABLE_CRITICAL_EXTENSION = 12;
	public const LDAP_CONFIDENTIALITY_REQUIRED = 13;
	public const LDAP_SASL_BIND_IN_PROGRESS = 14;
	public const LDAP_NO_SUCH_ATTRIBUTE = 16;
	public const LDAP_UNDEFINED_TYPE = 17;
	public const LDAP_INAPPROPRIATE_MATCHING = 18;
	public const LDAP_CONSTRAINT_VIOLATION = 19;
	public const LDAP_TYPE_OR_VALUE_EXISTS = 20;
	public const LDAP_INVALID_SYNTAX = 21;
	public const LDAP_NO_SUCH_OBJECT = 32;
	public const LDAP_ALIAS_PROBLEM = 33;
	public const LDAP_INVALID_DN_SYNTAX = 34;
	public const LDAP_IS_LEAF = 35;
	public const LDAP_ALIAS_DEREF_PROBLEM = 36;
	public const LDAP_INAPPROPRIATE_AUTH = 48;
	public const LDAP_INVALID_CREDENTIALS = 49;
	
	// Active directory specific codes equivalent to 49
	public const AD_INVALID_CREDENTIALS = '52e';
	public const AD_USER_NOT_FOUND = 525;
	public const AD_NOT_PERMITTED_TO_LOGON_AT_THIS_TIME = 530;
	public const AD_RESTRICTED_TO_SPECIFIC_MACHINES = 531;
	public const AD_PASSWORD_EXPIRED = 532;
	public const AD_ACCOUNT_DISABLED = 533;
	public const AD_ERROR_TOO_MANY_CONTEXT_IDS = 568;
	public const AD_ACCOUNT_EXPIRED = 701;
	public const AD_USER_MUST_RESET_PASSWORD = 773;
	
	// Other ldap errors
	public const LDAP_INSUFFICIENT_ACCESS = 50;
	public const LDAP_BUSY = 51;
	public const LDAP_UNAVAILABLE = 52;
	public const LDAP_UNWILLING_TO_PERFORM = 53;
	public const LDAP_LOOP_DETECT = 54;
	public const LDAP_NAMING_VIOLATION = 64;
	public const LDAP_OBJECT_CLASS_VIOLATION = 65;
	public const LDAP_NOT_ALLOWED_ON_NONLEAF = 66;
	public const LDAP_NOT_ALLOWED_ON_RDN = 67;
	public const LDAP_ALREADY_EXISTS = 68;
	public const LDAP_NO_OBJECT_CLASS_MODS = 69;
	public const LDAP_RESULTS_TOO_LARGE = 70;
	public const LDAP_AFFECTS_MULTIPLE_DSAS = 71;
	public const LDAP_OTHER = 80;
	
	// Ldap user errors
	public const LDAP_ERROR_GENEREL = 10000;
	public const LDAP_ERROR_MAL_FORMED_URL = 10001;
	public const LDAP_ERROR_UNAUTHENTICATED_BIND = 10002;
	public const LDAP_ERROR_COMMUNICATION_EXCEPTION = 10300;
	public const LDAP_ERROR_SOCKET_TIMEOUT = 10301;
	public const LDAP_ERROR_CONNECTION_REFUSED = 10302;
	public const LDAP_ERROR_CONNECTION_RESET = 10303;
	public const LDAP_ERROR_NO_ROUTE = 10304;
	public const LDAP_ERROR_UNKNOW_HOST = 10305;
	public const LDAP_ERROR_SSL_EXCEPTION = 10400;
	public const LDAP_ERROR_SSL_EMPTY_CERT_STORE = 10401;
	public const LDAP_ERROR_SSL_CERT_NOT_FOUND = 10402;
	public const LDAP_ERROR_SSL_CERT_EXPIRED = 10403;
	public const LDAP_ERROR_INVALID_SEARCH_FILTER_EXCEPTION = 10500;
	
	/**
	 * Gets whether the connection is open. Returns true if the connection is
	 * active, i.e. if the try to connect has already been done.
	 * 
	 * @return boolean
	 */
	public function isOpen() : bool;
	
	/**
	 * Opens the connection to the server. This will pass the state of the
	 * connection from "not connected" to "connected" or "connection failed"
	 * according to ldap connection binding results. This function binds with
	 * the ldap according to the username and password given in the config
	 * files. If none are given, the bind is anonymous.
	 * 
	 * @return boolean if the connexion is opened
	 * @throws LdapThrowable if there is no LDAP php extension
	 * @throws LdapThrowable if the connection fails to open
	 */
	public function open() : bool;
	
	/**
	 * Gets whether this connexion is currently binded (even if it is with 
	 * no login).
	 * 
	 * @return boolean
	 */
	public function isBinded() : bool;
	
	/**
	 * Gets the current login identifier this connection is binded with.
	 * Defaults to null when this connexion is not binded.
	 * 
	 * @return ?string
	 */
	public function isLoggedWith() : ?string;
	
	/**
	 * Authenticates to the server as given user. Use null login to
	 * authenticate as anonymous user. Opens the connexion if it is not
	 * currently opened.
	 * 
	 * @param string $login
	 * @param string $password
	 * @return boolean
	 * @throws LdapThrowable if the is not LDAP php extension
	 * @throws LdapThrowable if the connection fails to open
	 */
	public function login(?string $login = null, ?string $password = null) : bool;
	
	/**
	 * Counts the objects that matches the given criteria into the ldap.
	 * 
	 * @param LdapCriteriaInterface $criteria
	 * @return integer
	 * @throws LdapThrowable if the connection fails to open
	 */
	public function countObjects(LdapCriteriaInterface $criteria) : int;
	
	/**
	 * Counts the number of entries that matches the given criteria into the ldap.
	 * 
	 * @param LdapCriteriaInterface $criteria
	 * @return integer
	 * @throws LdapThrowable if the connection fails to open
	 */
	public function countEntries(LdapCriteriaInterface $criteria) : int;
	
	/**
	 * Searches the ldap for the requested objects that matches the given criteria.
	 * 
	 * @param LdapCriteriaInterface $criteria
	 * @return LdapObjectIteratorInterface<LdapRecordInterface>
	 * @throws LdapThrowable if the connection fails to open
	 */
	public function findObjects(LdapCriteriaInterface $criteria) : LdapObjectIteratorInterface;
	
	/**
	 * Searches the ldap for the requested entries that matches the given criteria.
	 * 
	 * @param LdapCriteriaInterface $criteria
	 * @return LdapEntryIteratorInterface
	 * @throws LdapThrowable if the connection fails to open
	 */
	public function findEntries(LdapCriteriaInterface $criteria) : LdapEntryIteratorInterface;
	
}
