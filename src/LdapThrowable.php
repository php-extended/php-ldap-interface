<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Throwable;

/**
 * LdapThrowable class file.
 * 
 * This throwable does nothing special but helps differenciate with generic
 * exceptions given by other libraries.
 * 
 * @author Anastaszor
 */
interface LdapThrowable extends Throwable
{
	
	// nothing to add
	
}
