<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Stringable;

/**
 * LdapObjectFactoryInterface interface file.
 * 
 * An LdapObjectFactoryInterface is an object which transforms raw results
 * from the ldap_get_attributes and ldap_get_dn functions into an object which
 * follows the relational model of the database inducted by the ldap.
 * 
 * Such factory will transform the data into an object, but is lacking the
 * information on how discriminate data such as to retrieve the right model 
 * class for the given ldap object.
 * 
 * @author Anastaszor
 */
interface LdapObjectFactoryInterface extends Stringable
{
	
	/**
	 * Builds the record according to the information given by the entry.
	 * If the class of target object can't be guessed, an exception will
	 * be thrown.
	 *
	 * @param LdapEntryInterface $entry
	 * @return LdapRecordInterface
	 * @throws LdapThrowable if the record is not recognized or cannot be
	 *                       built for any other reason
	 */
	public function buildObjectEntry(LdapEntryInterface $entry) : LdapRecordInterface;
	
}
