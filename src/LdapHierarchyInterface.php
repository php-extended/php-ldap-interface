<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-ldap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Ldap;

use Countable;
use Iterator;
use Stringable;

/**
 * LdapHierarchyInterface interface file.
 * 
 * This interface specifies a hierarchy of elements. A Hierarchy is a suite of
 * specific records that are children of one another until the top (most higher)
 * record is reached. Neighbor records of the records that are in the hierarchy
 * may be also present.
 * 
 * @author Anastaszor
 */
interface LdapHierarchyInterface extends Countable, Stringable
{
	
	/**
	 * Gets the shallow object, i.e. the least profound record in the hierarchy
	 * chain.
	 * 
	 * @return LdapRecordInterface
	 */
	public function getShallow() : LdapRecordInterface;
	
	/**
	 * Gets the target, i.e. the most profound record in the hierarchy chain.
	 * 
	 * @return LdapRecordInterface
	 */
	public function getTarget() : LdapRecordInterface;
	
	/**
	 * Gets all the hierarchy levels, in order, from the higher to the record
	 * that triggered this hierarchy.
	 * 
	 * @return Iterator<LdapHierarchyLevelInterface<LdapRecordInterface>>
	 */
	public function getHierarchyLevels() : Iterator;
	
}
